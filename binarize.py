from typing import Union, Optional, Tuple

import tensorflow as tf


@tf.autograph.experimental.do_not_convert
def approximately_binarize_tensor(
    t: tf.Tensor,
    zeta: Union[tf.Tensor, tf.Variable],
    apply_tanh: bool,
    axis: Optional[Union[Tuple[int, ...], int]] = None,
) -> tf.Tensor:
    """
    Given an input tensor this function applies approximate quantization of the inputs to the values {-1,1}.
    Args:
        t: Tensor to approximately binarize.
        zeta: Control the spread of the approximately binarized tensor. Increase zeta to reduce the variance of the
            approximately binarized outputs.
        apply_tanh: Set to True to restrict the inputs to the range (-1,1) using the tanh function prior to rescaling
            and shifting when approximately binarizing.
        axis: The axis along to group the tensor elements for the purpose of mean reduction and shifting.
    Returns:
        The approximately binarized tensor.
    """
    if apply_tanh:
        t = tf.keras.activations.tanh(x=t)

    where_t_plus: tf.Tensor = tf.greater(x=t, y=0.0)
    num_plus_col: tf.Tensor = tf.reduce_sum(
        input_tensor=tf.cast(x=where_t_plus, dtype=t.dtype), axis=axis, keepdims=True
    )
    where_t_plus_idx: tf.Tensor = tf.where(where_t_plus)
    num_where_t_plus: tf.Tensor = tf.reduce_sum(tf.cast(where_t_plus, dtype=tf.int32))

    where_t_minus: tf.Tensor = tf.less_equal(x=t, y=0.0)
    where_t_minus_idx: tf.Tensor = tf.where(where_t_minus)
    num_where_t_minus: tf.Tensor = tf.reduce_sum(tf.cast(where_t_minus, dtype=tf.int32))

    t_plus: tf.Tensor = tf.tensor_scatter_nd_update(
        tensor=t,
        indices=where_t_minus_idx,
        updates=tf.zeros(shape=num_where_t_minus),
    )

    t_plus_mean: tf.Tensor = tf.math.divide_no_nan(
        x=tf.reduce_sum(t_plus, axis=axis, keepdims=True), y=num_plus_col
    )

    multiples: tf.Tensor = tf.shape(t) + 1 - tf.shape(t_plus_mean)
    plus_offsets: tf.Tensor = tf.tile(input=t_plus_mean, multiples=multiples)
    plus_offsets = tf.tensor_scatter_nd_update(
        tensor=plus_offsets,
        indices=where_t_minus_idx,
        updates=tf.zeros(shape=num_where_t_minus),
    )

    offsets: tf.Tensor

    t_minus: tf.Tensor = tf.tensor_scatter_nd_update(
        tensor=t,
        indices=where_t_plus_idx,
        updates=tf.zeros(shape=num_where_t_plus),
    )

    num_minus_col: tf.Tensor = tf.reduce_sum(
        input_tensor=tf.cast(x=where_t_minus, dtype=t.dtype), axis=axis, keepdims=True
    )

    t_minus_mean: tf.Tensor = tf.math.divide_no_nan(
        x=tf.reduce_sum(t_minus, axis=axis, keepdims=True), y=num_minus_col
    )

    minus_offsets: tf.Tensor = tf.tile(input=t_minus_mean, multiples=multiples)
    minus_offsets = tf.tensor_scatter_nd_update(
        tensor=minus_offsets,
        indices=where_t_plus_idx,
        updates=tf.zeros(shape=num_where_t_plus),
    )

    offsets = plus_offsets + minus_offsets

    t -= offsets
    t *= tf.exp(-zeta)

    t = tf.tensor_scatter_nd_add(
        tensor=t,
        indices=where_t_plus_idx,
        updates=tf.ones(shape=num_where_t_plus),
    )

    t = tf.tensor_scatter_nd_add(
        tensor=t,
        indices=where_t_minus_idx,
        updates=-tf.ones(shape=num_where_t_minus),
    )

    return t
