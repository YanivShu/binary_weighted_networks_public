import numpy as np
from dataclasses import dataclass
from typing import List, Callable, Optional, Union

import tensorflow as tf


@dataclass
class Config:
    """
    Contains hyperparameter and other configuration options for an experiment.
    """

    mode: str
    reference_learning_rate_init: float
    reference_batch_size: int
    batch_size_gpu: int
    weight_decay_init: Optional[float]
    apply_tanh: bool
    alpha_steps: Optional[float]
    zeta_steps_start: Optional[float]
    zeta_steps_end: Optional[float]
    zeta_target: Optional[float]
    schedule_ratio: Union[Optional[List[float]], np.ndarray]
    schedule_rate: float
    get_optimizer: Callable[..., tf.keras.optimizers.Optimizer]
    num_epochs: int
    dtype: tf.DType = tf.float32
    tensorboard_update_freq: int = 50
