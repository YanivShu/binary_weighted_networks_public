from typing import Optional, Any, Dict

import tensorflow as tf

from layers import Conv2dBinary, DenseBinary
from models import BinaryWeightedNetworkBase


class LeNet5Binary(BinaryWeightedNetworkBase):
    def __init__(
        self,
        num_classes: int,
        mode: str,
        apply_tanh: bool,
        alpha_steps: Optional[int],
        zeta_steps_start: Optional[int],
        zeta_steps_end: Optional[int],
        zeta_final: Optional[int],
        tensorboard_update_freq: int = 50,
        dtype: Optional[tf.DType] = None,
        name: Optional[str] = None,
        **kwargs: Any,
    ) -> None:
        """
        Args:
            num_classes: The number of classes in the dataset.
            mode: One of 'float', 'binary'. Use binary to constrain the weights to be approximately {-alpha, alpha}
                during training and exactly one of {-1, 1} in inference.
            apply_tanh: Set to True to restrict the weights to the range (-1,1) using the tanh function prior to
                rescaling and shifting when approximately binarizing.
            alpha_steps: The number of training steps for alpha to reach value of 1 by linear interpolation.
            zeta_steps_start: The training step from which to start increasing zeta from an initial value of 1.
            zeta_steps_end: The training step which zeta will reach its target value.
            zeta_final: The final value of zeta.
            dtype: Parameters and output data type.
            name: Model's name.
            **kwargs: Additional arguments for base keras.Model.
        """
        super(LeNet5Binary, self).__init__(
            num_classes=num_classes,
            mode=mode,
            apply_tanh=apply_tanh,
            alpha_steps=alpha_steps,
            zeta_steps_start=zeta_steps_start,
            zeta_steps_end=zeta_steps_end,
            zeta_final=zeta_final,
            tensorboard_update_freq=tensorboard_update_freq,
            dtype=dtype,
            name=name,
            **kwargs,
        )

        self._my_layers.append(
            Conv2dBinary(
                filters=20,
                kernel_size=5,
                strides=1,
                padding="valid",
                mode=mode,
                apply_tanh=apply_tanh,
                dtype=dtype,
                name="conv2d_binary_0",
            )
        )

        self._my_layers.append(tf.keras.layers.MaxPooling2D())

        self._my_layers.append(
            tf.keras.layers.Activation(
                activation="relu",
                name=f"relu_0",
            )
        )

        self._my_layers.append(
            Conv2dBinary(
                filters=50,
                kernel_size=5,
                strides=1,
                padding="valid",
                mode=mode,
                apply_tanh=apply_tanh,
                dtype=dtype,
                name="conv2d_binary_1",
            )
        )

        self._my_layers.append(tf.keras.layers.MaxPooling2D())

        self._my_layers.append(
            tf.keras.layers.Activation(
                activation="relu",
                name=f"relu_1",
            )
        )

        self._my_layers.append(tf.keras.layers.Flatten())

        self._my_layers.append(
            DenseBinary(
                units=500,
                mode=mode,
                apply_tanh=apply_tanh,
                name="dense_binary_2",
            )
        )

        self._my_layers.append(
            tf.keras.layers.Activation(
                activation="relu",
                name=f"relu_2",
            )
        )

        self._my_layers.append(
            tf.keras.layers.Dense(
                units=num_classes, activation="linear", name="dense_logits"
            )
        )

    def get_config(self) -> Dict[str, Any]:
        return super(LeNet5Binary, self).get_config()
