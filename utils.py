from pathlib import Path
from typing import List, Optional

import tensorflow as tf
from tensorflow.python.keras.callbacks import ProgbarLogger, TerminateOnNaN


def get_alpha_for_step(
    step: tf.Tensor, alpha_steps: tf.Tensor, dtype: tf.DType
) -> tf.Tensor:
    """
    Calculates alpha for the current training step by means of linear interpolation so that that for training step
    alpha_steps and above alpha is 1.
    Args:
        step: The current training step.
        alpha_steps: The number of training steps for alpha to reach value of 1 by linear interpolation.
        dtype: The datatype fpr the returned tensor.

    Returns:
        The alpha value for the specified training step.
    """
    step = tf.cast(step, dtype=dtype)
    alpha_steps = tf.cast(alpha_steps, dtype=dtype)

    if alpha_steps == 0.0:
        return tf.convert_to_tensor(value=1, dtype=dtype)

    return tf.clip_by_value(
        t=tf.math.divide(x=step, y=alpha_steps),
        clip_value_min=0.0,
        clip_value_max=1.0,
    )


def get_zeta_for_step(
    step: tf.Tensor,
    zeta_steps_start: Optional[tf.Tensor],
    zeta_steps_end: Optional[tf.Tensor],
    zeta_final: Optional[tf.Tensor],
    dtype: tf.DType,
) -> tf.Tensor:
    """
    Calculates zeta for the current training step by means of linear interpolation from 1 to zeta_final over the range
    specified by [zeta_steps_start, zeta_steps_end].
    Args:
        step: The current training step.
        zeta_steps_start: The training step in which zeta starts being interpolated.
        zeta_steps_end: The training step in which zeta reaches it final value.
        zeta_final: The final value of zeta.
        dtype: The data type fpr the returned tensor.

    Returns:
        The zeta value for the specified training step.
    """
    if zeta_steps_start is None or zeta_steps_end is None or zeta_final is None:
        return tf.constant(value=1, dtype=dtype)

    step = tf.cast(step, dtype=dtype)
    zeta_steps_start = tf.cast(zeta_steps_start, dtype=dtype)
    zeta_steps_end = tf.cast(zeta_steps_end, dtype=dtype)
    zeta_final = tf.cast(zeta_final, dtype=dtype)

    return tf.constant(value=1, dtype=dtype) + (zeta_final - 1) * tf.math.divide_no_nan(
        x=tf.maximum(step - zeta_steps_start, 0), y=zeta_steps_end - zeta_steps_start
    )


def get_distributed_strategy(distribute: bool) -> tf.distribute.Strategy:
    """
    Args:
        distribute: True to indicate that distribution is desirable if possible.

    Returns:
        If distribute is True returns the mirrored distributed strategy for training on the same machine if multiple
        GPUs are detected. Otherwise the default strategy is returned.
    """
    strategy: tf.distribute.Strategy = tf.distribute.get_strategy()

    if distribute and len(tf.config.list_physical_devices(device_type="GPU")) > 1:
        strategy = tf.distribute.MirroredStrategy()

    print(
        f"Using distribute strategy {strategy} with number of devices: {strategy.num_replicas_in_sync}"
    )

    return strategy


def calculate_learning_rate(
    reference_learning_rate: float, reference_batch_size: int, batch_size: int
) -> float:
    """
    Args:
        reference_learning_rate: The learning rate that is applied when the batch size is equal to reference_batch_size.
        reference_batch_size: The batch size to use for the reference_learning_rate.
        batch_size: The actual batch size used for training.

    Returns:
        The initial learning rate to use at the end of the warmup stage.
    """
    return batch_size / reference_batch_size * reference_learning_rate


def calculate_batch_size(batch_size_gpu: int, num_gpus: int) -> int:
    """
    Args:
        batch_size_gpu: batch size to use on each of the avaiable GPUs.
        num_gpus: The number of GPUs available.

    Returns:
        The total batch size to use across all available GPUs.
    """
    return batch_size_gpu * num_gpus


def get_callbacks(
    checkpoint_path: Path,
    tensorboard_summaries_path: Path,
    tensorboard_update_freq: int = 50,
) -> List[tf.keras.callbacks.Callback]:
    """
    Creates training callbacks:
        - Write checkpoint files monitoring the maximal validation_accuracy achieved
        - Callback to save tensorboard summaries
        - Stateful batch statistics.
        - Terminate on NAN.
    Args:
        checkpoint_path: The path of the callback files including the directory and the file pattern.
        tensorboard_summaries_path: The path where to write TensorBoard summaries.
        tensorboard_update_freq: How many training steps to run before writing subsequent tensorboard summaries.

    Returns:
        The list of training callbacks.
    """
    checkpoint_callback: tf.keras.callbacks.ModelCheckpoint = (
        tf.keras.callbacks.ModelCheckpoint(
            filepath=checkpoint_path,
            monitor="val_acc",
            verbose=1,
            save_best_only=True,
            save_weights_only=True,
            mode="max",
        )
    )

    tensorboard_callback: tf.keras.callbacks.TensorBoard = (
        tf.keras.callbacks.TensorBoard(
            log_dir=tensorboard_summaries_path,
            histogram_freq=1,
            write_graph=False,
            write_images=False,
            update_freq=tensorboard_update_freq,
            profile_batch=0,
            embeddings_freq=0,
        )
    )

    return [
        checkpoint_callback,
        tensorboard_callback,
        ProgbarLogger(
            count_mode="steps",
            stateful_metrics=[
                "batch_acc",
                "batch_ce",
                "batch_loss",
            ],
        ),
        TerminateOnNaN(),
    ]


def calculate_steps_from_schedule_ratio(
    num_epochs: int, steps_per_epoch: int, schedule: Optional[float]
) -> Optional[int]:
    """
    Args:
        num_epochs: The total number of training epochs.
        steps_per_epoch: The number of training steps in an epoch.
        schedule: The fraction of all training steps.

    Returns:
        The number of training steps required to reach scheduled training steps.
    """
    if schedule is None:
        return None
    return int(num_epochs * steps_per_epoch * schedule)
