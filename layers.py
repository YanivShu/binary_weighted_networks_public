"""
Implementation of the binary dense and conv2d layers as described in the paper
'Exact Backpropagation in Binary Weighted Networks with Group Weight Transformations' https://arxiv.org/abs/2107.01400
"""
import abc
from typing import Optional, Union, Sequence, Tuple, Any, Callable

import tensorflow as tf
import tensorflow.keras as keras

from binarize import approximately_binarize_tensor


class BinaryLayerBase(abc.ABC, keras.layers.Layer):
    def __init__(
        self,
        mode: str,
        apply_tanh: bool,
        dtype: Optional[tf.DType] = None,
        name: Optional[str] = None,
        **kwargs: Any,
    ) -> None:
        """
        Args:
            mode: One of 'float', 'binary'. Use binary to progressively constrain the weights to be approximately in
                {-alpha, alpha} during training and exactly one of {-1, 1} in inference.
            apply_tanh: Set to True to restrict the weights to the range (-1,1) using the tanh function prior to
                rescaling and shifting when approximately binarizing.
            dtype: Parameters and output data type.
            name: layers' name.
            **kwargs: Additional parameters for keras.layers.Layer.
        """
        super(BinaryLayerBase, self).__init__(name=name, dtype=dtype, **kwargs)

        self._mode: Union[str, Callable[[int], str]] = (
            mode.lower() if isinstance(mode, str) else mode
        )

        if self._mode not in ("float", "binary"):
            raise ValueError("Mode must be one of 'float' or 'binary'")

        self._apply_tanh: bool = apply_tanh
        self._w: Optional[tf.Variable] = None
        self._binarized_w: Optional[tf.Variable] = None

    @abc.abstractmethod
    def build(self, input_shape: tf.TensorShape) -> None:
        """
        Creates the variables of the layer.
        Args:
            input_shape: The shape of the inputs.
        """
        pass

    @abc.abstractmethod
    def call(self, inputs, **kwargs: Any) -> tf.Tensor:
        """
        Forward propagation.
        Args:
            inputs: Batch of features.
            **kwargs: Additional keyword arguments for layers' call method.

        Returns:
            Forward propagation results.
        """
        pass

    def is_binary_mode(self) -> bool:
        """
        Returns:
            True if the layer is configured to operate in binary weights mode.
        """
        return self._mode == "binary"

    def write_summaries(self, step: tf.Tensor) -> None:
        """
        Writes TensorBoard summaries concerning the distribution of the binarized weights if the layer is in binary
        mode.
        Args:
            step: The training step.
        """
        if self.is_binary_mode() and self._binarized_w is not None:
            tf.summary.histogram(
                name=f"{self._name}/batch_binarized_w",
                data=self._binarized_w,
                step=step,
            )
            tf.summary.scalar(
                name=f"{self._name}/batch_binarized_w_max",
                data=tf.reduce_max(input_tensor=self._binarized_w),
                step=step,
            )
            tf.summary.scalar(
                name=f"{self._name}/batch_binarized_w_min",
                data=tf.reduce_min(input_tensor=self._binarized_w),
                step=step,
            )
            tf.summary.scalar(
                name=f"{self._name}/batch_binarized_w_mean",
                data=tf.reduce_mean(input_tensor=self._binarized_w),
                step=step,
            )


class DenseBinary(BinaryLayerBase):
    """
    Implementation of the approximately binary weighted Dense layer without bias.
    """

    def __init__(
        self,
        units: int,
        mode: str,
        apply_tanh: bool,
        dtype: Optional[tf.DType] = None,
        name: Optional[str] = None,
        **kwargs: Any,
    ) -> None:
        """

        Args:
            units: The number of units, the dimensionality of the output space.
            mode: One of 'float', 'binary'. Use binary to constrain the weights to be approximately in {-alpha, alpha}
                during training and exactly one of {-1, 1} in inference.
            apply_tanh: Set to True to restrict the weights to the range (-1,1) using the tanh function prior to
                rescaling and shifting when approximately binarizing.
            dtype: Parameters and output data type.
            name: layers' name.
            **kwargs: Additional parameters for BinaryLayerBase.
        """
        super(DenseBinary, self).__init__(
            mode=mode,
            apply_tanh=apply_tanh,
            dtype=dtype,
            name=name,
            **kwargs,
        )
        self._units: int = units

    def build(self, input_shape: tf.TensorShape) -> None:
        """
        Creates the variables of the layer.
        Args:
            input_shape: The shape of the inputs.
        """
        fan_in: int = int(input_shape[-1])
        initializer_w: tf.keras.initializers.GlorotUniform = (
            tf.keras.initializers.GlorotUniform()
        )
        values: tf.Tensor = initializer_w(shape=(fan_in, self._units), dtype=self.dtype)
        self._w = tf.Variable(initial_value=values, trainable=True, name="w")
        self._binarized_w = tf.Variable(
            initial_value=tf.zeros_like(input=values, dtype=self.dtype),
            trainable=False,
            name="binarized_w",
        )

    def call(self, inputs: tf.Tensor, **kwargs: Any) -> tf.Tensor:
        """
        Forward propagation. In training uses the algorithm described in the paper to approximately binarize the weights
        and then calculate a weighted mean of the binarized and original weights. The interpolated value is then used
        to calculate the dot product with the inputs. In inference the method simply uses sign thresholding to binarize
        the weights before proceeding to calculate the dot product.
        Args:
            inputs: Batch of features.
            **kwargs:
                training, a boolean that is True when training and False in inference.
                alpha, a float tensor indicating the alpha value for the training step.
                zeta, a float tensor indicating zeta's value for the training step.

        Returns:
            The matrix multiplication of the optionally binarized weights with the inputs.
        """
        training: bool = kwargs["training"]
        alpha: tf.Tensor = kwargs["alpha"]
        zeta: tf.Tensor = kwargs["zeta"]
        w: tf.Tensor = self._w.read_value()

        if self.is_binary_mode():
            if training:
                w = approximately_binarize_tensor(
                    t=w,
                    zeta=zeta,
                    apply_tanh=self._apply_tanh,
                    axis=0,
                )
                w = alpha * w + (1.0 - alpha) * self._w.read_value()
            else:
                w = tf.sign(w)

            self._binarized_w.assign(w)

        return inputs @ w


class Conv2dBinary(BinaryLayerBase):
    """
    Implementation of the approximately binary weighted Conv2D layer without bias.
    """

    def __init__(
        self,
        filters: int,
        kernel_size: Union[int, Sequence[int]],
        strides: Union[int, Sequence[int]],
        padding: str,
        mode: str,
        apply_tanh: bool,
        dtype: Optional[tf.DType] = None,
        name: Optional[str] = None,
        **kwargs: Any,
    ) -> None:
        """

        Args:
            filters: The number of filters for the layer, the dimensionality of the output space.
            kernel_size: An integer or tuple/list of 2 integers, specifying the height and width of the 2D convolution
                window. Can be a single integer to specify the same value for all spatial dimensions.
            strides: An integer or tuple/list of 2 integers, specifying the strides of the convolution along the height
                and width. Can be a single integer to specify the same value for all spatial dimensions.
            padding: Either the string 'SAME' or 'VALID' indicating the type of padding algorithm to use. 'VALID' means
                no padding, 'SAME' results in padding evenly to the left/right or up/down of the input such that output
                has the same height/width dimension as the input.
            mode: One of 'float', 'binary'. Use binary to constrain the weights to be approximately in {-alpha, alpha}
                during training and exactly one of {-1, 1} in inference.
            apply_tanh: Set to True to restrict the weights to the range (-1,1) using the tanh function prior to
                rescaling and shifting when approximately binarizing.
            dtype: Parameters and output data type.
            name: Layers' name.
            **kwargs: Additional arguments for BinaryLayerBase.
        """
        super(Conv2dBinary, self).__init__(
            mode=mode,
            apply_tanh=apply_tanh,
            dtype=dtype,
            name=name,
            **kwargs,
        )

        if isinstance(kernel_size, int):
            kernel_size = (kernel_size, kernel_size)

        if isinstance(strides, int):
            strides = (strides, strides)

        self._filters: int = filters
        self._kernel_size: Tuple[int, int] = tuple(kernel_size)
        self._strides: Tuple[int, int] = tuple(strides)
        self._padding: str = padding.upper()

    def build(self, input_shape: tf.TensorShape) -> None:
        """
        Creates the variables of the layer.
        Args:
            input_shape: The shape of the inputs.
        """

        initializer_w: tf.keras.initializers.GlorotUniform = (
            tf.keras.initializers.GlorotUniform()
        )

        values: tf.Tensor = initializer_w(
            shape=self._kernel_size + (input_shape[-1], self._filters),
            dtype=self.dtype,
        )

        self._w = tf.Variable(initial_value=values, trainable=True, name="w")
        self._binarized_w = tf.Variable(
            initial_value=tf.zeros_like(input=values, dtype=self.dtype),
            trainable=False,
            name="binarized_w",
        )

    def call(self, inputs: tf.Tensor, **kwargs: Any) -> tf.Tensor:
        """
        Forward propagation. In training uses the algorithm described in the paper to approximately binarize the weights
        and then calculate a weighted mean of the binarized and original weights. The interpolated value is then used
        to calculate the dot product with the inputs. In inference the method simply uses sign thresholding to binarize
        the weights before proceeding to calculate the dot product.
        Args:
            inputs: Batch of features.
            **kwargs:
                training, a boolean that is True when training and False in inference.
                alpha, a float tensor indicating the alpha value for the training step.
                zeta, a float tensor indicating zeta's value for the training step.

        Returns:
            Standard convolution of the inputs and the binarized weights.
        """
        training: bool = kwargs["training"]
        alpha: tf.Tensor = kwargs["alpha"]
        zeta: tf.Tensor = kwargs["zeta"]

        w: tf.Tensor = self._w.read_value()

        if self.is_binary_mode():
            if training:
                prev_shape: tf.TensorShape = w.shape
                w = tf.reshape(
                    tensor=w,
                    shape=(
                        self._kernel_size[0] * self._kernel_size[1] * inputs.shape[-1],
                        self._filters,
                    ),
                )

                w = approximately_binarize_tensor(
                    t=w,
                    zeta=zeta,
                    apply_tanh=self._apply_tanh,
                    axis=0,
                )

                w = tf.reshape(tensor=w, shape=prev_shape)
                w = alpha * w + (1.0 - alpha) * self._w.read_value()
            else:
                w = tf.sign(w)

            self._binarized_w.assign(w)

        return tf.nn.conv2d(
            input=inputs, filters=w, strides=self._strides, padding=self._padding
        )


class ResidualIdentityBlockBinary(BinaryLayerBase):
    """
    A binary weighted revision of the B(3,3) block of the wide residual network with identity mappings.
    See http://www.bmva.org/bmvc/2016/papers/paper087/paper087.pdf.
    """

    def __init__(
        self,
        filters: int,
        down_sample: bool,
        mode: str,
        apply_tanh: bool,
        dtype: Optional[tf.DType] = None,
        name: Optional[str] = None,
        **kwargs: Any,
    ) -> None:
        """
        Args:
            filters: The number of filters in each of the Conv2D layers.
            down_sample: True to down sample by half the spatial dimensionality of the inputs.
            mode: One of 'float', 'binary'. Use binary to constrain the weights to be approximately {-alpha, alpha}
                during training and exactly one of {-1, 1} in inference. This is applied to all the layers in the block.
            apply_tanh: Set to True to restrict the weights to the range (-1,1) using the tanh function prior to
                rescaling and shifting when approximately binarizing. This is applied to all the layers in the block.
            dtype: The dtype for the block, all weights, variables and outputs.
            name: Block's name.
            **kwargs: Additional arguments for tf.keras.Layer.
        """
        super(ResidualIdentityBlockBinary, self).__init__(
            mode=mode,
            apply_tanh=apply_tanh,
            dtype=dtype,
            name=name,
            **kwargs,
        )

        self._filters: int = filters
        self._down_sample: bool = down_sample

        self._bn_0: tf.keras.layers.BatchNormalization = (
            tf.keras.layers.BatchNormalization(name=f"{self.name}/bn_0")
        )

        self._activation_0: tf.keras.layers.Layer = tf.keras.layers.Activation(
            activation="relu",
            name=f"{self.name}/relu_0",
        )

        self._conv_0: Conv2dBinary = Conv2dBinary(
            filters=self._filters,
            kernel_size=3,
            strides=2 if self._down_sample else 1,
            padding="same",
            mode=mode,
            apply_tanh=apply_tanh,
            dtype=dtype,
            name=f"{self.name}/conv_0",
            **kwargs,
        )

        self._bn_1: tf.keras.layers.BatchNormalization = (
            tf.keras.layers.BatchNormalization(name=f"{self.name}/bn_1")
        )

        self._activation_1: tf.keras.layers.Layer = tf.keras.layers.Activation(
            activation="relu",
            name=f"{self.name}/relu_1",
        )

        self._conv_1: Conv2dBinary = Conv2dBinary(
            filters=self._filters,
            kernel_size=3,
            strides=1,
            padding="same",
            mode=mode,
            apply_tanh=apply_tanh,
            dtype=dtype,
            name=f"{self.name}/conv_1",
            **kwargs,
        )

    def build(self, input_shape: tf.TensorShape) -> None:
        """
        Does nothing.
        Args:
            input_shape: The shape of the inputs.
        """
        pass

    def call(self, inputs: tf.Tensor, **kwargs: Any) -> tf.Tensor:
        """
        Forward propagation.
        Args:
            inputs: Mini-batch of activations.
            **kwargs:
                training, a boolean that is True when training and False in inference.
                alpha, a float tensor indicating the alpha value for the training step.
                zeta, a float tensor indicating zeta's value for the training step.

        Returns:
            Activations for this block.
        """
        training: bool = kwargs["training"]

        x: tf.Tensor = inputs
        x = self._bn_0(inputs=x, training=training)
        x = self._activation_0(inputs=x, training=training)
        x = self._conv_0(inputs=x, **kwargs)
        x = self._bn_1(inputs=x, training=training)
        x = self._activation_1(inputs=x, training=training)
        x = self._conv_1(inputs=x, **kwargs)

        if self._down_sample:
            inputs = tf.nn.avg_pool2d(input=inputs, ksize=2, strides=2, padding="SAME")

        if (self._filters - tf.shape(input=inputs)[-1]) > 0:
            inputs = tf.pad(
                tensor=inputs,
                paddings=[
                    [0, 0],
                    [0, 0],
                    [0, 0],
                    [0, self._filters - tf.shape(input=inputs)[-1]],
                ],
            )

        return x + inputs

    def write_summaries(self, step: tf.Tensor) -> None:
        """
        Writes tensorboard summaries for the binary convolution layers of this block.
        Args:
            step: The training step.
        """
        self._conv_0.write_summaries(step=step)
        self._conv_1.write_summaries(step=step)
