from typing import Any, Dict, Callable, Union, List

import tensorflow as tf
from tensorflow_addons.optimizers import SGDW

from config import Config


class Sgdw(SGDW):
    """
    implementation of the weight decay decoupled SGD that excludes batch norm parameters from weight decay.
    """

    def _decay_weights_op(self, var, apply_state=None):
        if "bn_" in var.name and ("/gamma:0" in var.name or "/beta:0" in var.name):
            return tf.no_op()

        return super(Sgdw, self)._decay_weights_op(var=var, apply_state=apply_state)


class LinearWarmUp(tf.keras.optimizers.schedules.LearningRateSchedule):
    """
    Linear warmup schedule.
    """

    def __init__(
        self,
        learning_rate_init: float,
        decay_schedule_fn: Union[
            tf.keras.optimizers.schedules.LearningRateSchedule,
            Callable[[tf.Tensor], tf.Tensor],
        ],
        warmup_steps: int,
        dtype: tf.DType = tf.float32,
        name: str = "LinearWarmUp",
    ) -> None:
        """
        Args:
            learning_rate_init: The learning rate when warmup stage ends.
            decay_schedule_fn: The learning rate scheduler post-warmup.
            warmup_steps: The warmup length in training steps.
            dtype: The data type of the learning rate tensor returned.
            name: An optional name for this object.
        """
        super().__init__()
        self._learning_rate_init: float = learning_rate_init

        self._decay_schedule_fn: Union[
            tf.keras.optimizers.schedules.LearningRateSchedule,
            Callable[[tf.Tensor], tf.Tensor],
        ] = decay_schedule_fn

        self._warmup_steps: int = warmup_steps
        self._dtype: tf.DType = dtype
        self._name: str = name

    def __call__(self, step: tf.Tensor) -> tf.Tensor:
        """
        Args:
            step: The training step.

        Returns:
            The learning rate for the training step.
        """
        step_dtype: tf.DType = step.dtype
        step = tf.cast(x=step, dtype=self._dtype)
        warmup_percent_done: tf.Tensor = step / self._warmup_steps
        warmup_learning_rate: tf.Tensor = self._learning_rate_init * warmup_percent_done
        return tf.cond(
            pred=step < self._warmup_steps,
            true_fn=lambda: warmup_learning_rate,
            false_fn=lambda: self._decay_schedule_fn(
                tf.cast(step - self._warmup_steps, dtype=step_dtype)
            ),
        )

    def __str__(self) -> str:
        return (
            f"LinearWarmUp {self._name} initial_learning_rate: {self._learning_rate_init}, "
            f"warmup_steps: {self._warmup_steps}, decay_schedule_fn: {self._decay_schedule_fn}."
        )

    def get_config(self) -> Dict[str, Any]:
        """
        Returns:
            Parameters required to re-instantiate a LinearWarmUp object.
        """
        return {
            "learning_rate_init": self._learning_rate_init,
            "decay_schedule_fn": self._decay_schedule_fn,
            "warmup_steps": self._warmup_steps,
            "dtype": self._dtype,
            "name": self._name,
        }


def get_sgdw_optimizer(
    conf: Config,
    steps_per_epoch: int,
    learning_rate_init: float,
    dtype: tf.DType,
    warmup_epochs: int = 5,
) -> SGDW:
    """
    Args:
        conf: The configuration object for the experiment.
        learning_rate_init: The initial learning rate.
        steps_per_epoch: The number of training steps in an epoch.
        dtype: The data type for the optimizer ops and variables.
        warmup_epochs: The number of epochs for the warmup stage to last.

    Returns:
        A weight decay decoupled SGD momentum optimizer that excludes batch norm parameters from weight decay. Learning
        rate and weight decay updates schedules are created as per the provided configuration and a learning rate warmup
        schedule.
    """
    boundaries: List[int] = [
        int(steps_per_epoch * s * conf.num_epochs) for s in conf.schedule_ratio
    ]

    learning_rates: List[float] = [
        learning_rate_init * conf.schedule_rate ** i
        for i in range(len(conf.schedule_ratio) + 1)
    ]
    weight_decays: List[float] = [
        conf.weight_decay_init * conf.schedule_rate ** i
        for i in range(len(conf.schedule_ratio) + 1)
    ]
    learning_rate_fn: tf.keras.optimizers.schedules.PiecewiseConstantDecay = (
        tf.keras.optimizers.schedules.PiecewiseConstantDecay(
            boundaries=boundaries, values=learning_rates
        )
    )
    weight_decay_fn: tf.keras.optimizers.schedules.PiecewiseConstantDecay = (
        tf.keras.optimizers.schedules.PiecewiseConstantDecay(
            boundaries=boundaries, values=weight_decays
        )
    )

    print(
        f"Learning rates {learning_rate_fn.values} with boundaries {learning_rate_fn.boundaries}"
    )
    print(
        f"Weight decay {weight_decay_fn.values} with boundaries {weight_decay_fn.boundaries}"
    )

    return Sgdw(
        weight_decay=weight_decay_fn,
        learning_rate=LinearWarmUp(
            learning_rate_init=learning_rate_init,
            decay_schedule_fn=learning_rate_fn,
            warmup_steps=warmup_epochs * steps_per_epoch,
            dtype=dtype,
            name="learning_rate_warmup",
        ),
        momentum=0.9,
        nesterov=False,
    )
