## Exact Backpropagation in Binary Weighted Networks with Group Weight Transformations
### What is this repository for?

This repository provides a TensorFlow implementation of the method described in the paper "Exact Backpropagation in Binary Weighted Networks with Group Weight Transformations" for training binary weighted neural networks. The paper is available at https://arxiv.org/abs/2107.01400. This repository is NOT maintained and is provided as is for the purpose of recreating the results reported in the paper.

### How do I get set up?
There are two options to set up:
#### Docker
1. Source configure.sh 
2. Build the docker image using docker or docker-compose.
3. Run the container. If docker-compose is available then the script docker_compose_bash.sh can be used to conveniently launch bash in the container.

#### Virtual environment
Install the packages specified in requirements.txt using your favourite package manager.

### Running the experiments
Run the *_demo.py scripts. Datasets are automatically downloaded and installed locally therefore an internet connection is required. Artefacts are written to the out/ directory and can be visualised using TensorBoard.

### Feedback
If you notice any issue or have any feedback please email to the address specified in the paper https://arxiv.org/abs/2107.01400