FROM tensorflow/tensorflow:2.5.0-gpu

ARG DEBIAN_FRONTEND=noninteractive
RUN echo 'debconf debconf/frontend select Noninteractive' | debconf-set-selections
RUN apt-get update && \
    apt-get install -y -q --no-install-recommends apt-utils && \
    apt-get clean -y && \
    rm -rf /var/lib/apt/lists/*
RUN python -m pip install pip==21.0.1
RUN python -m pip install tensorflow-addons tensorflow-datasets
ARG PROJECT_DIR=/opt/project
RUN mkdir -p $PROJECT_DIR
RUN useradd --create-home dockeruser
USER dockeruser
WORKDIR $PROJECT_DIR
