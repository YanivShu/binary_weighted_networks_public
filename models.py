from typing import Optional, Any, List, Dict

import tensorflow as tf

from layers import BinaryLayerBase
from utils import get_alpha_for_step, get_zeta_for_step


class BinaryWeightedNetworkBase(tf.keras.Model):
    """
    Base class for all binary weighted networks
    """

    def __init__(
        self,
        num_classes: int,
        mode: str,
        apply_tanh: bool,
        alpha_steps: int,
        zeta_steps_start: Optional[int],
        zeta_steps_end: Optional[int],
        zeta_final: Optional[int],
        tensorboard_update_freq: int = 50,
        dtype: Optional[tf.DType] = None,
        name: Optional[str] = None,
        **kwargs: Any,
    ) -> None:
        """
        Args:
            num_classes: The number of classes in the dataset.
            mode: One of 'float', 'binary'. Use binary to constrain the weights to be approximately in {-1, 1} during
                training and exactly one of these values in inference.
            apply_tanh: Set to True to restrict the weights to the range (-1,1) using the tanh function prior to
                rescaling and shifting when approximately binarizing.
            alpha_steps: The number of training steps for alpha to reach value of 1 by linear interpolation.
            zeta_steps_start: The training step from which to start increasing zeta from an initial value of 1.
            zeta_steps_end: The training step which zeta will reach its target value.
            zeta_final: The final value of zeta.
            tensorboard_update_freq: The number of steps between batch summaries.
            dtype: The dtype for the model, layers and outputs.
            name: Model's name.
            **kwargs: Additional arguments for tf.keras.Model.
        """
        super(BinaryWeightedNetworkBase, self).__init__(
            dtype=dtype, name=name, **kwargs
        )
        self._num_classes: int = num_classes
        self._mode: str = mode
        self._apply_tanh: bool = apply_tanh
        self._alpha_steps: tf.Tensor = tf.convert_to_tensor(
            0 if alpha_steps is None else alpha_steps, dtype=tf.int64
        )
        self._zeta_steps_start: Optional[tf.Tensor] = (
            None
            if zeta_steps_start is None
            else tf.convert_to_tensor(value=zeta_steps_start, dtype=tf.int64)
        )
        self._zeta_steps_end: Optional[tf.Tensor] = (
            None
            if zeta_steps_end is None
            else tf.convert_to_tensor(value=zeta_steps_end, dtype=tf.int64)
        )
        self._zeta_final: Optional[tf.Tensor] = (
            None
            if zeta_final is None
            else tf.convert_to_tensor(value=zeta_final, dtype=dtype)
        )
        self._tensorboard_update_freq: int = tensorboard_update_freq

        self._my_layers: List[tf.keras.layers.Layer] = []

    def call(
        self,
        inputs: tf.Tensor,
        training: bool = False,
        mask: Optional[tf.Tensor] = None,
        **kwargs,
    ) -> tf.Tensor:
        """
        Forward propagation.
        Args:
            inputs: Batch of images for classification.
            training: True when training, False when in inference.
            mask: Unused.
            **kwargs: Additional keyword arguments for layers' call method.

        Returns:
            Logits for class predictions for each image in the batch.
        """
        step: tf.Tensor = self.optimizer.iterations.value()
        alpha: tf.Tensor = get_alpha_for_step(
            step=step, alpha_steps=self._alpha_steps, dtype=self.dtype
        )
        zeta: tf.Tensor = get_zeta_for_step(
            step=step,
            zeta_steps_start=self._zeta_steps_start,
            zeta_steps_end=self._zeta_steps_end,
            zeta_final=self._zeta_final,
            dtype=self.dtype,
        )
        x: tf.Tensor = inputs
        for i, l in enumerate(self._my_layers):
            if isinstance(l, BinaryLayerBase):
                x = l(inputs=x, training=training, alpha=alpha, zeta=zeta, **kwargs)
            else:
                x = l(inputs=x, training=training, **kwargs)

        if step % self._tensorboard_update_freq == 0:
            self.write_summaries(step=step)

        return x

    def write_summaries(self, step: tf.Tensor) -> None:
        """
        Writes TensorBoard summaries of the model and comprising layers.
        Args:
            step: The training/test step
        """
        if self._mode == "binary":
            tf.summary.scalar(
                name="alpha",
                data=get_alpha_for_step(
                    step=step, alpha_steps=self._alpha_steps, dtype=self.dtype
                ),
                step=step,
            )
            tf.summary.scalar(
                name="zeta",
                data=get_zeta_for_step(
                    step=step,
                    zeta_steps_start=self._zeta_steps_start,
                    zeta_steps_end=self._zeta_steps_end,
                    zeta_final=self._zeta_final,
                    dtype=self.dtype,
                ),
                step=step,
            )

        for l in self._my_layers:
            if isinstance(l, BinaryLayerBase):
                l.write_summaries(step=step)

    def get_config(self) -> Dict[str, Any]:
        return {
            "num_classes": self._num_classess,
            "mode": self._mode,
            "zeta_steps_start": None
            if self._zeta_steps_start is None
            else int(self._zeta_steps_start.numpy()),
            "zeta_steps_end": None
            if self._zeta_steps_end is None
            else int(self._zeta_steps_end.numpy()),
            "zeta_final": None
            if self._zeta_final is None
            else int(self._zeta_final.numpy()),
            "apply_tanh": self._apply_tanh,
            "alpha_steps": self._alpha_steps,
            "tensorboard_update_freq": self._tensorboard_update_freq,
            **super(BinaryWeightedNetworkBase, self).get_config(),
        }
