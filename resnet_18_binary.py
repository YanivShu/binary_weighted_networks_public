from typing import Optional, Any, Dict, Tuple

import tensorflow as tf

from layers import Conv2dBinary, ResidualIdentityBlockBinary
from models import BinaryWeightedNetworkBase


class ResidualNetwork18Binary(BinaryWeightedNetworkBase):
    """
    Binary weighted version of the ResNet18 model with parameter free identity mappings.
    """

    def __init__(
        self,
        num_classes: int,
        mode: str,
        apply_tanh: bool,
        alpha_steps: int,
        zeta_steps_start: Optional[int],
        zeta_steps_end: Optional[int],
        zeta_final: Optional[int],
        tensorboard_update_freq: int = 50,
        dtype: Optional[tf.DType] = None,
        name: Optional[str] = None,
        **kwargs: Any,
    ) -> None:
        """
        Args:
            num_classes: The number of classes in the dataset.
            mode: One of 'float', 'binary'. Use binary to constrain the weights to be approximately {-alpha, alpha}
                during training and exactly one of {-1, 1} in inference.
            apply_tanh: Set to True to restrict the weights to the range (-1,1) using the tanh function prior to
                rescaling and shifting when approximately binarizing.
            alpha_steps: The number of training steps for alpha to reach value of 1 by linear interpolation.
            zeta_steps_start: The training step from which to start increasing zeta from an initial value of 1.
            zeta_steps_end: The training step which zeta will reach its target value.
            zeta_final: The final value of zeta.
            tensorboard_update_freq: The number of steps between batch summaries.
            dtype: The dtype for the model, layers and outputs.
            name: Model's name.
            **kwargs: Additional arguments for tf.keras.Model.
        """
        super(ResidualNetwork18Binary, self).__init__(
            num_classes=num_classes,
            mode=mode,
            apply_tanh=apply_tanh,
            alpha_steps=alpha_steps,
            zeta_steps_start=zeta_steps_start,
            zeta_steps_end=zeta_steps_end,
            zeta_final=zeta_final,
            tensorboard_update_freq=tensorboard_update_freq,
            dtype=dtype,
            name=name,
            **kwargs,
        )

        self._my_layers.append(tf.keras.layers.BatchNormalization(name="bn_inputs"))

        self._my_layers.append(
            Conv2dBinary(
                filters=64,
                kernel_size=3,
                strides=1,
                padding="same",
                mode="float",
                apply_tanh=apply_tanh,
                dtype=dtype,
                name="conv2d_binary_0",
                **kwargs,
            )
        )

        filters_in_group: Tuple[int, ...] = (64, 128, 256, 512)

        for g in range(4):
            for i in range(2):
                self._my_layers.append(
                    ResidualIdentityBlockBinary(
                        filters=filters_in_group[g],
                        down_sample=g > 0 and i == 0,
                        mode=mode,
                        apply_tanh=apply_tanh,
                        dtype=dtype,
                        name=f"res_block_group_{g}_block_{i}",
                        **kwargs,
                    )
                )

        self._my_layers.append(tf.keras.layers.BatchNormalization(name="bn_last"))

        self._my_layers.append(
            tf.keras.layers.Activation(
                activation="relu",
                name=f"relu_last",
            )
        )

        self._my_layers.append(tf.keras.layers.Flatten())

        self._my_layers.append(tf.keras.layers.Dropout(rate=0.2))

        self._my_layers.append(
            tf.keras.layers.Dense(
                units=num_classes, activation="linear", name="dense_logits"
            )
        )

    def get_config(self) -> Dict[str, Any]:
        return super(ResidualNetwork18Binary, self).get_config()
