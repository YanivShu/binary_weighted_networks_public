#!/bin/bash

BINARY_USER="$(id -u):$(id -g)"
BINARY_REPO_DIR=$(git rev-parse --show-toplevel)

export NVIDIA_VISIBLE_DEVICES=all
export BINARY_IMAGE_TAG=1.0
export BINARY_USER
export BINARY_REPO_DIR
