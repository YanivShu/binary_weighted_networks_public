from pathlib import Path
from typing import List, Dict, Optional

import numpy as np
import tensorflow as tf

import tensorflow_datasets as tfds
from config import Config
from datasets import load_cifar
from optimizers import get_sgdw_optimizer
from resnet_18_binary import ResidualNetwork18Binary
from utils import (
    get_distributed_strategy,
    get_callbacks,
    calculate_batch_size,
    calculate_learning_rate,
    calculate_steps_from_schedule_ratio,
)

tests_dir: Path = Path(__file__).parent.joinpath("out", "resnet18", "cifar10")


def main() -> None:
    distribute: bool = False
    dtype: tf.DType = tf.float32

    tests: Dict[str, Config] = dict(
        binary_weights=Config(
            mode="binary",
            reference_learning_rate_init=0.05,
            reference_batch_size=128,
            batch_size_gpu=128,
            weight_decay_init=1e-3,
            apply_tanh=False,
            alpha_steps=0.9,
            zeta_steps_start=0.9,
            zeta_steps_end=1.0,
            zeta_target=12,
            schedule_ratio=np.linspace(0.1, 1, num=6, endpoint=False),
            schedule_rate=0.3,
            get_optimizer=get_sgdw_optimizer,
            num_epochs=400,
        ),
        float=Config(
            mode="float",
            reference_learning_rate_init=0.1,
            reference_batch_size=128,
            batch_size_gpu=128,
            weight_decay_init=5e-4,
            apply_tanh=False,
            alpha_steps=None,
            zeta_steps_start=None,
            zeta_steps_end=None,
            zeta_target=None,
            schedule_ratio=np.asarray(a=(1 / 3, 2 / 3)),
            schedule_rate=0.1,
            get_optimizer=get_sgdw_optimizer,
            num_epochs=300,
        ),
    )

    suite: List[str] = [k for k in tests.keys()]

    for test in suite:
        conf: Config = tests[test]
        print(f"Configuration options: {conf}")

        model_dir: Path = tests_dir.joinpath(test)
        tf.keras.backend.set_floatx(str(conf.dtype).split("'")[1])

        distribute_strategy: tf.distribute.Strategy = get_distributed_strategy(
            distribute=distribute
        )

        batch_size: int = calculate_batch_size(
            batch_size_gpu=conf.batch_size_gpu,
            num_gpus=distribute_strategy.num_replicas_in_sync,
        )

        ds_train: tf.data.Dataset
        ds_test: tf.data.Dataset
        ds_info: tfds.core.DatasetInfo
        ds_train, ds_test, ds_info = load_cifar(
            batch_size=batch_size, dtype=dtype, ten=True
        )
        steps_per_epoch: int = ds_info.splits["train"].num_examples // batch_size + 1

        learning_rate_init: float = calculate_learning_rate(
            reference_learning_rate=conf.reference_learning_rate_init,
            reference_batch_size=conf.reference_batch_size,
            batch_size=batch_size,
        )

        with distribute_strategy.scope():
            model: ResidualNetwork18Binary = ResidualNetwork18Binary(
                num_classes=10,
                mode=conf.mode,
                apply_tanh=conf.apply_tanh,
                alpha_steps=calculate_steps_from_schedule_ratio(
                    num_epochs=conf.num_epochs,
                    steps_per_epoch=steps_per_epoch,
                    schedule=conf.alpha_steps,
                ),
                zeta_steps_start=calculate_steps_from_schedule_ratio(
                    num_epochs=conf.num_epochs,
                    steps_per_epoch=steps_per_epoch,
                    schedule=conf.zeta_steps_start,
                ),
                zeta_steps_end=calculate_steps_from_schedule_ratio(
                    num_epochs=conf.num_epochs,
                    steps_per_epoch=steps_per_epoch,
                    schedule=conf.zeta_steps_end,
                ),
                zeta_final=conf.zeta_target,
                dtype=conf.dtype,
            )

            model.compile(
                optimizer=conf.get_optimizer(
                    conf=conf,
                    steps_per_epoch=steps_per_epoch,
                    learning_rate_init=learning_rate_init,
                    dtype=dtype,
                ),
                loss=tf.keras.losses.SparseCategoricalCrossentropy(from_logits=True),
                metrics=["acc", "ce"],
            )

            checkpoint_dir: Path = model_dir.joinpath("checkpoints")
            latest_checkpoint: Optional[str] = tf.train.latest_checkpoint(
                checkpoint_dir=checkpoint_dir
            )
            if latest_checkpoint is not None:
                print(f"Resuming training from checkpoint {latest_checkpoint}")
                model.load_weights(filepath=latest_checkpoint)

            callbacks: List[tf.keras.callbacks.Callback] = get_callbacks(
                checkpoint_path=model_dir.joinpath(
                    "checkpoints", "{epoch:03d}_{val_acc:.4f}.ckpt"
                ),
                tensorboard_summaries_path=model_dir.joinpath("summaries"),
            )

        model.fit(
            x=ds_train,
            batch_size=batch_size,
            epochs=conf.num_epochs,
            verbose=1,
            callbacks=callbacks,
            validation_data=ds_test,
        )

        print(f"Finished test {test}")
        tf.keras.backend.clear_session()


if __name__ == "__main__":
    main()
